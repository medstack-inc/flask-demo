from flask import Flask, request, redirect, render_template
from werkzeug.serving import run_simple

from flask_sqlalchemy import SQLAlchemy

import sqlalchemy
from sqlalchemy.exc import IntegrityError

from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired

from os import environ


import json

with open('/config_db.json') as json_file:
    data = json.load(json_file)

#DB_NAME = environ.get("DB_NAME", "testdb_dev")
#DB_HOST = environ.get("DB_HOST", "postgresql")

DB_NAME=data["DB_NAME"]
DB_USER=data["DB_USER"]
DB_PASSWORD=data["DB_PASSWORD"]
DB_HOST=data["DB_HOST"]
DB_SSL=data["DB_SSL"]

def make_db_uri(db_name):
    database_uri = 'postgresql+psycopg2://{dbuser}:{dbpassword}@{dbhost}:{dbport}/{dbname}'.format(
            dbuser=DB_USER,
            dbname=DB_NAME,
            dbpassword=DB_PASSWORD,
            dbhost=DB_HOST,
            dbport=5432
            )
    return database_uri

def create_db():
    engine = sqlalchemy.create_engine(make_db_uri("postgres"), connect_args={'sslmode':DB_SSL})
    conn = engine.connect()
    conn.execute("commit")
    conn.execute("create database %s" % DB_NAME)
    conn.close()

try:
    create_db()
except:
    pass

app = Flask(__name__)

app.config.update(
    SQLALCHEMY_DATABASE_URI=make_db_uri(DB_NAME),
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
)

app.config["SECRET_KEY"] = "changeme"

db = SQLAlchemy(app)

class MyForm(FlaskForm):
    name = StringField("Enter your name", validators=[DataRequired()])

class Signature(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    def __repr__(self):
        return '<Signature %r>' % self.name

db.create_all()

@app.route("/view_book", methods=("GET", "POST"))
def index():
    return render_template("show_names.html", names=(name.name for name in Signature.query.all()))

@app.route("/", methods=("GET", "POST"))
def add_name():
    form = MyForm()
    if form.validate_on_submit():
        db.session.add(Signature(name=form.name.data))
        try:
            db.session.commit()
        except IntegrityError:
            return redirect("/view_book")
        return redirect("/view_book")
    return render_template("add_name.html", form=form)

if __name__ == "__main__":
    run_simple("0.0.0.0", 80, app, use_reloader=True)
